﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Security;
using KPIAdmin.Models;
using System.Data.Entity;

namespace KPIAdmin.Controllers
{
    public class MenuController : KPIController
    {

        private KPI_SecurityEntities_Menu db = new KPI_SecurityEntities_Menu();
        



        // GET: Menu
        public ActionResult Menu()
        {
            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }

            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View();
            };
            return View(db.ApplicationMenus.ToList());
        }



        // GET: Menu/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationMenu applicationMenu = db.ApplicationMenus.Find(id);
            if (applicationMenu == null)
            {
                return HttpNotFound();
            }
            return View(applicationMenu);
        }
              // GET: Menu/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Menu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AMAPID,AMSequence,AMKEID,AMStatus,AMParentID,AMID,AMName,AMDefaultPage,AMHelpPage,AMUserName,AMDateAdd,AMDateChange,AMDateDelete,AMAccessLevel,AMTitle,AMKeyTypeID,AMUrl,AMSequenceMajor,xxxAMImageUrl,xxxAMKEID")] ApplicationMenu applicationMenu)
        {
            if (ModelState.IsValid)
            {
                applicationMenu.AMID = Guid.NewGuid();
                db.ApplicationMenus.Add(applicationMenu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationMenu);
        }

        // GET: Menu/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationMenu applicationMenu = db.ApplicationMenus.Find(id);
            if (applicationMenu == null)
            {
                return HttpNotFound();
            }
            return View(applicationMenu);
        }

        // POST: Menu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AMAPID,AMSequence,AMKEID,AMStatus,AMParentID,AMID,AMName,AMDefaultPage,AMHelpPage,AMUserName,AMDateAdd,AMDateChange,AMDateDelete,AMAccessLevel,AMTitle,AMKeyTypeID,AMUrl,AMSequenceMajor,xxxAMImageUrl,xxxAMKEID")] ApplicationMenu applicationMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationMenu);
        }

        // GET: Menu/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationMenu applicationMenu = db.ApplicationMenus.Find(id);
            if (applicationMenu == null)
            {
                return HttpNotFound();
            }
            return View(applicationMenu);
        }

        // POST: Menu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ApplicationMenu applicationMenu = db.ApplicationMenus.Find(id);
            db.ApplicationMenus.Remove(applicationMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
