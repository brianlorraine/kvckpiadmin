﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Security;
using KPIAdmin.Models;
using System.Data.Entity;

namespace KPIAdmin.Controllers
{
    public class AdminController : KPIController
    {
        private KPI_SecurityEntities db = new KPI_SecurityEntities();
        private KPI_SecurityEntitiesKeyType dbtypes = new KPI_SecurityEntitiesKeyType();

        [HttpGet]
        [Authorize]
        public ActionResult Apps()
        {
            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }
            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View();
            }
            var q = db.Applications.ToList();
            return View(q);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Apps([Bind(Include = "APID,APSequence,APKEID,APStatus,APName,APMenuName,APDefaultPage,APHelpPage,APUserAdminPage,APLGID,APLogo,APVersion,APHasUserSecurity,APUserName,APDateAdd,APDateChange,APDateDelete,APKeyTypeID,APHasSecurityCode,APAccessLevel,APDefaultFolder,APLogoPath,APNameAlt,APAltUserPage")] Application application,string myaction, string referrer, Guid? APID)
        {
            if (myaction == "Details")
            {
                if (APID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Application app = db.Applications.Find(APID);
                if (app == null)
                {
                    return HttpNotFound();
                }
                return View(app);
            } else if (myaction == "Edit")
            {
                if (referrer=="Edit")
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            application.APUserName = Session["KPISystemUserName"].ToString();
                            application.APDateChange = DateTime.Now;
                            db.Entry(application).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;

                    }
                    application.KeyTypeItems = dbtypes.ApplicationKeyTypes.Select(c => new SelectListItem { Value = c.KEID.ToString(), Text = c.KEID.ToString() }).ToList();
                    return View(application);
                } else
                {
                    ModelState.Clear();
                    if (APID == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Application app= db.Applications.Find(APID);
                    if (app == null)
                    {
                        return HttpNotFound();
                    }
                    app.KeyTypeItems = dbtypes.ApplicationKeyTypes.Select(c => new SelectListItem { Value = c.KEID.ToString(), Text = c.KEID.ToString() }).ToList();
                    return View(app);
                }
                
            } else if (APID == null)
            {
                using (db)
                {
                    var model = new Application
                    {
                        KeyTypeItems = dbtypes.ApplicationKeyTypes.Select(c => new SelectListItem { Value = c.KEID.ToString(), Text = c.KEID.ToString() }).ToList()
                    };
                    return View(model);
                }
            } else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult CreateApp([Bind(Include = "APID,APSequence,APKEID,APStatus,APName,APMenuName,APDefaultPage,APHelpPage,APUserAdminPage,APLGID,APLogo,APVersion,APHasUserSecurity,APUserName,APDateAdd,APDateChange,APDateDelete,APKeyTypeID,APHasSecurityCode,APAccessLevel,APDefaultFolder,APLogoPath,APNameAlt,APAltUserPage")] Application application)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                application.APID = Guid.NewGuid();
            application.APUserName = Session["KPISystemUserName"].ToString();
            application.APDateAdd = DateTime.Now;
            application.APDateChange = DateTime.Now;
            application.APLogo = "0000.png";
            db.Applications.Add(application);
            db.SaveChanges();
            ViewBag.Message = application.APID.ToString() + " Added!";
            var mykeytypes = dbtypes.ApplicationKeyTypes.ToList();
            ViewBag.KeyTypes = mykeytypes;
                showMessageString = new
                {
                    param1 = 200,
                    param2 = application.APID

                };
            }
            catch (Exception ex)
            {
                
                ViewBag.Error = ex.Message;
                showMessageString = new
                {
                    param1 = 404,
                    param2 = ex.Message
                };

            }
            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }

}
