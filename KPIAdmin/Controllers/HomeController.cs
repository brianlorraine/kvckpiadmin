﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Security;
using KPIAdmin.Models;
using System.Data.Entity;

namespace KPIAdmin.Controllers
{

    public class HomeController : KPIController
    {

        private KPI_SecurityEntities db = new KPI_SecurityEntities();
        private KPI_SecurityEntitiesKeyType dbtypes = new KPI_SecurityEntitiesKeyType();
        private KPI_SecurityEntities_AppSecurity dbsecurity = new KPI_SecurityEntities_AppSecurity();
      

        [HttpGet]
        [Authorize]
        public ActionResult Default()
        {
            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }

            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View();
            };
            return View();
        }

        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            if (CSInclude.SSO_Check_LocalIP(Request.ServerVariables["REMOTE_ADDR"].ToString()) != 0)
            {
                if (Request.QueryString["ReturnUrl"] != null || Request.QueryString["ReturnID"] != null)
                {
                    if (Request.QueryString["ReturnID"] == null)
                    {
                        string SingleSignOnURL = System.Configuration.ConfigurationManager.AppSettings["KPISingleSignOnURL"].ToString();
                        string ReturnID = CSInclude.SSO_Create_Ticket(Request.ServerVariables["REMOTE_ADDR"].ToString(), CSInclude.Build_ApplicationPath(true)).ToString();
                        Response.Redirect(SingleSignOnURL + "?ReturnID=" + ReturnID + "&ReturnPath=" + CSInclude.Build_ApplicationPath(false));
                        Response.End();
                    }
                    else
                    {
                        string RtnKey = CSInclude.SSO_Delete_Ticket(Request.QueryString["ReturnID"]).ToString();
                        if (RtnKey != "")
                        {

                            FormsAuthentication.RedirectFromLoginPage(RtnKey, false);



                            Response.End();
                        }
                    }
                }
            }

            // If server variables are missing or there is no "ReturnURL" or "ReturnID" in the querystring, then something is wrong.
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }



    

}