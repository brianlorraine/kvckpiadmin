﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;

namespace KPIAdmin.Controllers
{
    public class KPIController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (actionName != "Login" || TempData.Peek("KeyType") == null)
            {

                if (Request.IsAuthenticated == false)
                {
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }

                if (Session["KPISystemUserID"] == null)
                {
                    //INT\BRLORRAI
                    string myname = System.Web.HttpContext.Current.User.Identity.Name;
                    KpiSecurity oKPISecurity = new KpiSecurity(myname, (Session["KPISystemIdentityAlt"] == null) ? "" : Session["KPISystemIdentityAlt"].ToString());


                    if (oKPISecurity.UserID == null)
                    {
                        ViewBag.Error = "Your user account is disabled in KPI System.<BR><BR>Please contact the Bethesda Help Desk at extention 8157.";

                    }
                    else
                    {
                        Session["KPISystemUserID"] = oKPISecurity.UserID;
                        Session["KPISystemUserName"] = oKPISecurity.FullName;
                        Session["KPISystemUserEmailAddress"] = oKPISecurity.EmailAddress;
                        Session["KPISystemUserApplication"] = oKPISecurity.Applications;

                        string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
                        string ConnString = "KPI_Security";
                        string SqlCmd = "KPISecurity2018.Application_CheckAccess";
                        SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
                        SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

                        try
                        {
                            oSqlConnection.Open();
                            oSqlCommand.CommandType = CommandType.StoredProcedure;
                            SqlCommandBuilder.DeriveParameters(oSqlCommand);
                            oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                            //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                            oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                            //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                            oSqlCommand.ExecuteNonQuery();

                            TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                            TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                            TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                            TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                            //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                            TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                            TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                            oSqlCommand.Dispose();
                            oSqlConnection.Close();
                            Session["OBLoggedIn"] = true;

                            switch (Convert.ToInt16(TempData.Peek("KeyType")))
                            {
                                case 1010:
                                    TempData["LevelMin"] = 50;
                                    break;
                                case 1020:
                                    TempData["LevelMin"] = 10;
                                    break;
                                case 1030:
                                    TempData["LevelMin"] = 10;
                                    break;
                                case 1060:
                                    TempData["LevelMin"] = 10;
                                    break;
                                case 6010:
                                    TempData["LevelMin"] = 10;
                                    break;
                                default:
                                    TempData["LevelMin"] = 10;
                                    break;
                            }



                        }
                        catch (Exception ex)
                        {
                            oSqlCommand.Dispose();
                            oSqlConnection.Close();
                            ViewBag.Error = ex.Message;
                        }

                    }
                }
            }
        }
    }
}