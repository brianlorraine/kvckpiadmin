﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Security;
using KPIAdmin.Models;
using System.Data.Entity;

namespace KPIAdmin.Controllers
{
    public class UserSecuritiesController : KPIController
    {
        private KPI_SecurityEntities_UserSecurity db = new KPI_SecurityEntities_UserSecurity();


        // GET: UserSecurities
        public ActionResult UserSecurity()
        {
            return View(db.UserSecurities.ToList());
        }
        
        // GET: UserSecurities/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSecurity userSecurity = db.UserSecurities.Find(id);
            if (userSecurity == null)
            {
                return HttpNotFound();
            }
            return View(userSecurity);
        }

        // GET: UserSecurities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserSecurities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "USAPID,USAUID,USObject,USKeyTypeID,USDateAdd,USDateChange,USDateDelete")] UserSecurity userSecurity)
        {
            if (ModelState.IsValid)
            {
                userSecurity.USAPID = Guid.NewGuid();
                db.UserSecurities.Add(userSecurity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userSecurity);
        }

        // GET: UserSecurities/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSecurity userSecurity = db.UserSecurities.Find(id);
            if (userSecurity == null)
            {
                return HttpNotFound();
            }
            return View(userSecurity);
        }

        // POST: UserSecurities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "USAPID,USAUID,USObject,USKeyTypeID,USDateAdd,USDateChange,USDateDelete")] UserSecurity userSecurity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userSecurity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userSecurity);
        }

        // GET: UserSecurities/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSecurity userSecurity = db.UserSecurities.Find(id);
            if (userSecurity == null)
            {
                return HttpNotFound();
            }
            return View(userSecurity);
        }

        // POST: UserSecurities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UserSecurity userSecurity = db.UserSecurities.Find(id);
            db.UserSecurities.Remove(userSecurity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
