﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KPIAdmin.Models;

namespace KPIAdmin.Controllers
{
    public class LevelsController : Controller
    {
        private KPI_SecurityEntities_Level db = new KPI_SecurityEntities_Level();

        // GET: Levels
        public ActionResult AppLevel()
        {
            return View(db.ApplicationLevels.ToList());
        }

        // GET: Levels/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationLevel applicationLevel = db.ApplicationLevels.Find(id);
            if (applicationLevel == null)
            {
                return HttpNotFound();
            }
            return View(applicationLevel);
        }

        // GET: Levels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Levels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LVAPID,LVLevel,LVDescription,LVRemark,LVUserName,LVDateAdd,LVDateChange,LVDateDelete")] ApplicationLevel applicationLevel)
        {
            if (ModelState.IsValid)
            {
                applicationLevel.LVAPID = Guid.NewGuid();
                db.ApplicationLevels.Add(applicationLevel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationLevel);
        }

        // GET: Levels/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationLevel applicationLevel = db.ApplicationLevels.Find(id);
            if (applicationLevel == null)
            {
                return HttpNotFound();
            }
            return View(applicationLevel);
        }

        // POST: Levels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LVAPID,LVLevel,LVDescription,LVRemark,LVUserName,LVDateAdd,LVDateChange,LVDateDelete")] ApplicationLevel applicationLevel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicationLevel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationLevel);
        }

        // GET: Levels/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationLevel applicationLevel = db.ApplicationLevels.Find(id);
            if (applicationLevel == null)
            {
                return HttpNotFound();
            }
            return View(applicationLevel);
        }

        // POST: Levels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ApplicationLevel applicationLevel = db.ApplicationLevels.Find(id);
            db.ApplicationLevels.Remove(applicationLevel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
