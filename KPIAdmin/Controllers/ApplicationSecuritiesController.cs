﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using PagedList;
using PagedList.Mvc;
using System.IO;
using System.IO.Compression;
using System.Web.Security;
using KPIAdmin.Models;
using System.Data.Entity;

namespace KPIAdmin.Controllers
{
    public class ApplicationSecuritiesController : KPIController
    {
        private KPI_SecurityEntities_AppSecurity db = new KPI_SecurityEntities_AppSecurity();
        private KPI_SecurityEntities_Level dblevels = new KPI_SecurityEntities_Level();
        private KPI_SecurityEntities dbapps = new KPI_SecurityEntities();
        private KPI_SecurityEntities_User dbuser = new KPI_SecurityEntities_User();

        // GET: ApplicationSecurities
        public ActionResult AppSecurity()
        {
         
            var applications = new SelectList(dbapps.Applications.OrderBy(s => s.APName).Select(c => c.APName).ToList());
            ViewBag.Applications = applications;

            var users = new SelectList(dbuser.DirectoryUser_Load(0, null).Select(i => new { Text=i.KYDescription, Value=i.KYID }),"Value","Text");
            ViewBag.Users = users;

            IEnumerable<vwApplicationSecurity> AppSec = db.vwApplicationSecurities.AsQueryable().ToList();

            int numreq = AppSec.Count();

            int pageSize = 25;
            int pageNumber = 1;
            decimal mypgct = numreq / Convert.ToDecimal(pageSize);
            int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
            ViewBag.mypagecount = mypagecount;

            var list = new List<SelectListItem>();

            for (int i = 1; i <= mypagecount; i++)
                list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

            ViewBag.list = list;
            ViewBag.pagenum = pageNumber.ToString();



            return View(AppSec.OrderBy(s => s.APName).ThenBy(s => s.AUFullName).ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult AppSecurity([Bind(Include = "SEAPID,SEAUID,SELevel,SEAdmin,SEFlag1,SEDefault,SESecurityCode,SEUserName,SEDateAdd,SEDateChange,SEDateDelete")] vwApplicationSecurity applicationSecurity, string myaction, string referrer, Guid? SEAPID, Guid? SEAUID, string Applications, string Users, string currentFilter, string CurrentFilterU,string whichpage, string whichpagenum, string MyPagedList)
        {
            if (myaction == "Details")
            {
                if (SEAPID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                vwApplicationSecurity app = db.vwApplicationSecurities.Find(SEAPID,SEAUID);
                if (app == null)
                {
                    return HttpNotFound();
                }
                return View(app);
            }
            else if (myaction == "Edit")
            {
                if (referrer == "Edit")
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            
                            applicationSecurity.SEDateChange = DateTime.Now;
                            db.Entry(applicationSecurity).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;

                    }
                    applicationSecurity.levelitems = dblevels.ApplicationLevels.Where(s => s.LVAPID == SEAPID).OrderBy(c => c.LVLevel).Select(c => new SelectListItem { Value = c.LVLevel.ToString(), Text = c.LVDescription.ToString() }).ToList();
                    return View(applicationSecurity);
                }
                else
                {
                    ModelState.Clear();
                    if (SEAPID == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    vwApplicationSecurity app = db.vwApplicationSecurities.Find(SEAPID,SEAUID);
                    if (app == null)
                    {
                        return HttpNotFound();
                    }
                    //app.levelitems = dblevels.ApplicationLevels.Where(s => s.LVAPID.Equals(SEAPID)).Select(c => new SelectListItem { Value = c.LVLevel.ToString(), Text = c.LVLevel.ToString() }).ToList();
                    app.levelitems = dblevels.ApplicationLevels.Where(s => s.LVAPID == SEAPID).OrderBy(c=> c.LVLevel).Select(c => new SelectListItem { Value = c.LVLevel.ToString(), Text = c.LVDescription.ToString() }).ToList();

                    return View(app);
                }

            }
            else if (SEAPID == null)
            {

                if (String.IsNullOrEmpty(Applications))
                {
                    Applications = currentFilter;
                }
                if (String.IsNullOrEmpty(Users))
                {
                    Users = CurrentFilterU;
                }

                ViewBag.CurrentFilter = Applications;
                ViewBag.CurrentFilterU = Users;

                var applications = new SelectList(dbapps.Applications.OrderBy(s => s.APName).Select(c => c.APName).ToList());
                ViewBag.Applications = applications;

                var users = new SelectList(dbuser.DirectoryUser_Load(0, null).Select(i => new { Text = i.KYDescription, Value = i.KYID }), "Value", "Text");
                ViewBag.Users = users;

                IEnumerable<vwApplicationSecurity> AppSec = db.vwApplicationSecurities.OrderBy(s => s.APName).ThenBy(s => s.AUFullName).AsQueryable().ToList();

                if (!String.IsNullOrEmpty(Applications))
                {
                   AppSec= AppSec.Where(s => s.APName.Equals(Applications));
                }
                if (!String.IsNullOrEmpty(Users))
                {
                    AppSec = AppSec.Where(s => s.SEAUID.Equals(new Guid(Users)));
                }

                int numreq = AppSec.Count();

                int pageSize = 25;
                int pageNumber = 1;
                decimal mypgct = numreq / Convert.ToDecimal(pageSize);
                int mypagecount = Convert.ToInt32(Math.Ceiling(mypgct));
                ViewBag.mypagecount = mypagecount;
                if (MyPagedList != "" && MyPagedList != null)
                {
                    pageNumber = Convert.ToInt32(MyPagedList);
                }
                else if (whichpagenum != "" && whichpagenum != null)
                {
                    pageNumber = Convert.ToInt32(whichpagenum);
                }
                if (pageNumber > mypagecount)
                {
                    pageNumber = 1;
                }
                switch (whichpage)
                {
                    case "Next":
                        pageNumber++;
                        break;
                    case "Last":
                        pageNumber = mypagecount;
                        break;
                    case "Previous":
                        pageNumber--;
                        break;
                    case "First":
                        pageNumber = 1;
                        break;
                }

                var list = new List<SelectListItem>();

                for (int i = 1; i <= mypagecount; i++)
                    list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

                ViewBag.list = list;
                ViewBag.pagenum = pageNumber.ToString();

                return View(AppSec.ToPagedList(pageNumber, pageSize));


            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }






        // GET: ApplicationSecurities/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vwApplicationSecurity applicationSecurity = db.vwApplicationSecurities.Find(id);
            if (applicationSecurity == null)
            {
                return HttpNotFound();
            }
            return View(applicationSecurity);
        }

        // GET: ApplicationSecurities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApplicationSecurities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SEAPID,SEAUID,SELevel,SEAdmin,SEFlag1,SEDefault,SESecurityCode,SEUserName,SEDateAdd,SEDateChange,SEDateDelete")] vwApplicationSecurity applicationSecurity)
        {
            if (ModelState.IsValid)
            {
                applicationSecurity.SEAPID = Guid.NewGuid();
                db.vwApplicationSecurities.Add(applicationSecurity);
                db.SaveChanges();
                
            }

            return View(applicationSecurity);
        }

        // GET: ApplicationSecurities/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vwApplicationSecurity applicationSecurity = db.vwApplicationSecurities.Find(id);
            if (applicationSecurity == null)
            {
                return HttpNotFound();
            }
            return View(applicationSecurity);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
