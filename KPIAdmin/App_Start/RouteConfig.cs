﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KPIAdmin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "maprouteLogin",
                url: "Login",
                defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteDefault",
                url: "Default",
                defaults: new { controller = "Home",action="Default", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteCreateApp",
                url: "CreateApp",
                defaults: new { controller = "Admin", action="CreateApp", id = UrlParameter.Optional }
            );

             routes.MapRoute(
                name: "maprouteEditApp",
                url: "CreateApp",
                defaults: new { controller = "Admin", action = "CreateApp", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteApps",
                url: "Apps",
                defaults: new { controller = "Admin", action = "Apps", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteApplicationSecurities",
                url: "AppSecurity",
                defaults: new { controller = "ApplicationSecurities", action = "AppSecurity", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteMenu",
                url: "Menu",
                defaults: new { controller = "Menu", action = "Menu", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteUserSecurity",
                url: "UserSecurity",
                defaults: new { controller = "UserSecurities", action = "UserSecurity", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "maprouteLevels",
                url: "AppLevel",
                defaults: new { controller = "Levels", action = "AppLevel", id = UrlParameter.Optional }
            );




        }
    }
}
