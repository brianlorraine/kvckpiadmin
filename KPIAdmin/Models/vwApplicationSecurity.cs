using System.ComponentModel.DataAnnotations;

namespace KPIAdmin.Models
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using KPIAdmin.Models;


    public partial class vwApplicationSecurity
    {
        public string APName { get; set; }
        public string AUFullName { get; set; }
        public byte SELevel { get; set; }
        public bool SEAdmin { get; set; }
        public bool SEFlag1 { get; set; }
        public bool SEDefault { get; set; }
        public byte[] SESecurityCode { get; set; }
        public System.DateTime SEDateAdd { get; set; }
        public System.DateTime SEDateChange { get; set; }
        public Nullable<System.DateTime> SEDateDelete { get; set; }
        public System.Guid SEAPID { get; set; }
        public System.Guid SEAUID { get; set; }
        public IEnumerable<SelectListItem> levelitems { get; set; }
        public string LVDescription { get; set; }
    }
}
